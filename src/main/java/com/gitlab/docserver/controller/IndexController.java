package com.gitlab.docserver.controller;

import com.gitlab.docserver.ApplicationProperties;
import com.gitlab.docserver.api.Cache;
import com.gitlab.docserver.api.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;


/**
 * Responsible for serving a list of artifacts with link to their documentation
 */
@Controller
public class IndexController {
    private final String VIEW_NAME = "index";

    @Autowired
    private ApplicationProperties docServerProperties;

    @Autowired
    private Cache cache;

    @RequestMapping("/")
    public String getDocIndex(Model model) {
        model.addAttribute("title", docServerProperties.getDocTitle());
        Collection<Group> groups = cache.getGroups();
        model.addAttribute("groups", groups);

        return VIEW_NAME;
    }

}
