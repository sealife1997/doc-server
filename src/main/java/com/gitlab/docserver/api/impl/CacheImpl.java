package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.ApplicationProperties;
import com.gitlab.docserver.api.Artifact;
import com.gitlab.docserver.api.ArtifactVersion;
import com.gitlab.docserver.api.Cache;
import com.gitlab.docserver.api.Group;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
@Slf4j
class CacheImpl implements Cache {
    private final ConcurrentMap<String, Group> groupByIdMapping = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationProperties applicationProperties;

    @Override
    public Group getGroup(String groupId) {
        if (!groupByIdMapping.containsKey(groupId)) {
            log.info("Add group {}", groupId);
            groupByIdMapping.put(groupId, new GroupImpl(groupId));
        }
        return groupByIdMapping.get(groupId);
    }

    @Override
    public Collection<Group> getGroups() {
        return groupByIdMapping.values().stream().sorted(GroupImpl::compareTo).collect(Collectors.toList());
    }

    @ServiceActivator(inputChannel = "cacheInputChannel", outputChannel = "nullChannel")
    public Artifact addArtifact(ArtifactFileInfo artifactFileInfo) {
        String groupId = artifactFileInfo.getGroupId();
        Group group = getGroup(groupId);

        String artifactId = artifactFileInfo.getArtifactId();
        Artifact artifact = group.getArtifact(artifactId);
        ((ArtifactImpl)artifact).addVersion(artifactFileInfo);
        return artifact;
    }

    @Scheduled(fixedDelayString = "${docserver.cache-cleanup-delay:2000}")
    protected void cacheCleanup() {
        groupByIdMapping.values().stream()
                .map(Group::getArtifacts)
                .flatMap(Collection::stream)
                .map(item -> (ArtifactImpl)item)
                .map(ArtifactImpl::getVersions)
                .flatMap(Collection::stream)
                .filter(this::hasCorrespondingFile)
                .forEach(this::cacheCleanup);


    }

    private void cacheCleanup(ArtifactVersion artifactVersion) {
        GroupImpl group = (GroupImpl) artifactVersion.getGroup();
        group.removeVersion(artifactVersion);

        if(group.getArtifacts().isEmpty()) {
            log.info("Remove artifact {}", artifactVersion);
            groupByIdMapping.remove(group.getGroupId());
        }
    }

    private boolean hasCorrespondingFile(ArtifactVersion version) {
        String docPath = applicationProperties.getDocPath();
        String path = docPath + File.separator + version.getPath();
        log.debug("Check artifact file {} for version {}", path, version);

        return !Files.exists(Paths.get(path));
    }

}
