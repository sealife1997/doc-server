package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.api.Artifact;
import com.gitlab.docserver.api.ArtifactVersion;

final class ArtifactUtil {
    private static final String PATH_SEP = ":";

    static String toString(Artifact artifact) {
        return artifact.getGroupId() + PATH_SEP + artifact.getArtifactId();
    }

    static String toString(ArtifactVersion artifactVersion) {
        return toString(artifactVersion.getArtifact()) + PATH_SEP + artifactVersion.getVersion();
    }

    private ArtifactUtil() {}
}
